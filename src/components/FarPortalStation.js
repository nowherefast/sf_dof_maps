import React from 'react';

/** CSS */
import '../css/Main.css'
import '../css/FarPortalStation.css'

/** REACT Bootstrap Stuff*/
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Form from  'react-bootstrap/Form';
import ListGroup from 'react-bootstrap/ListGroup';
import InputGroup from 'react-bootstrap/InputGroup';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Image from 'react-bootstrap/Image';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import ProgressBar from 'react-bootstrap/ProgressBar';

/** Images */
// Default Image
import FarPortalStationImg from '../img/far_portal_station/FarPortalStation.png';
// Map Images
import Cafeteria from '../img/far_portal_station/Cafeteria.png';
import CrewQuarters from '../img/far_portal_station/CrewQuarters.png';
import DockingRing from '../img/far_portal_station/DockingRing.png';
import GeneralStore from '../img/far_portal_station/GeneralStore.png';
import Greenhouse from '../img/far_portal_station/Greenhouse.png';
import Gym from '../img/far_portal_station/Gym.png';
import Hangar from '../img/far_portal_station/FarPortalStationHangar.png';
import Library from '../img/far_portal_station/Library.png';
import ObservationDeck from '../img/far_portal_station/ObservationDeck.png';
import Salon from '../img/far_portal_station/KasathanBarber.png';
import SmallBank from '../img/far_portal_station/SmallBank.png';
import Security from '../img/far_portal_station/Yutwoot.png';
 ////>> Labs
import EnvironmentSimulationLab from '../img/far_portal_station/EnvironmentResearchLab.png';
import MaterialsLab from '../img/far_portal_station/MaterialResearchLab.png';
import PortalConstructionLab from '../img/far_portal_station/PortalConstructionLab.PNG';
import SignalAnalysisLab from '../img/far_portal_station/SignalAnalysisLab.png';
// Secondary Images
import CerebricFungusImg from '../img/far_portal_station/CerebricFungus.png';





//TODO
/**
    Elevator
    Docking Ring [x]
    Hangar [x]
    General Store [x]
    Salon [x]
    Greenhouse [x]
    Lab Materials [x]
    Lab - Portal Construction [x]
    Cafeteria [x]
    Lab - Environment Simulation [x]
    Lab - Signal Analysis [x]
    Library [x]
    Security [x]
    Small Bank [x]
    Crew Quarters [x]
    Gym [x]
    Observation Room [x]
    Enginering & Cargo []
 */

const images = {
    /** Default */
    'farPortalStation': FarPortalStationImg,
    /** Main Images */
    'cafeteria': Cafeteria,
    'crewQuarters': CrewQuarters,
    'dockingRing': DockingRing,
    'generalStore': GeneralStore,
    'greenhouse': Greenhouse,
    'gym': Gym,
    'hangar': Hangar,
    'library': Library,
    'observationDeck': ObservationDeck,
    'salon': Salon,
    'smallBank': SmallBank,
    'security': Security,
    'labEnvironmentSimulation': EnvironmentSimulationLab,
    'labMaterials': MaterialsLab,
    'labPortalConstruction': PortalConstructionLab,
    'labSignalAnalysis': SignalAnalysisLab
    
    /** Secondary Images */
}
const CSSConstants = {
    'defaultSize':{    
        'width':'1048',
        'height':'650'
    },
    'squareSize':{    
        'width':'650',
        'height':'650'
    }

}

class FarPortalStation extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            'mainImg':'farPortalStation',
            'mainImgSize':'defaultImgSize' 
        };
    }
    handleDeckClick(imgName, e){
        let newState = this.state;
        newState.mainImg = imgName;
        if(imgName === 'smallBank' || imgName === 'security'){
            newState.mainImgSize = 'squareImgSize';
        } else {
            newState.mainImgSize = 'defaultImgSize';
        }
        this.setState(newState);
    }
    render() {
        return (
  
            <div className="mainComponent">
                <Row>
                    <Col md={8}>
                        <Image className={this.state.mainImgSize} src={images[this.state.mainImg]} />
                    </Col>
                    <Col md={4}>
                        <svg width="450" height="650" xmlns="http://www.w3.org/2000/svg">
                            <g>
                            <title>background</title>
                            <rect fill="#000000" id="canvas_background" height="650" width="450" y="-1" x="-1"/>
                            <g display="none" id="canvasGrid">
                            <rect fill="url(#gridpattern)" strokeWidth="0" y="0" x="0" height="100%" width="100%" id="svg_1"/>
                            </g>
                            </g>
                            <g>
                            <title>Layer 1</title>
                            <ellipse stroke="#000" ry="54.499998" rx="17" onClick={(e) => this.handleDeckClick('hangar', e)} name="hangar" id="fps_hangarId" cy="485.937503" cx="270.500001" strokeOpacity="null" strokeWidth="1.5" fill="#aad4ff"/>
                            <ellipse stroke="#000" ry="54.500004" rx="17" onClick={(e) => this.handleDeckClick('dockingRing', e)} name="dockingRing" id="fps_dockingRingId" cy="485.937496" cx="333.500001" strokeOpacity="null" strokeWidth="1.5" fill="#003f7f"/>
                            <path id="svg_46" d="m230.5,529.4375l-17.5,-26.4375l-128,3l-39,9l2,14l52,8l130.5,-7.5625z" fillOpacity="null" strokeOpacity="null" strokeWidth="1.5" stroke="#ffffff" fill="none"/>
                            <path id="svg_47" d="m226.5,494.4375l-17.5,-26.4375l-128,3l-39,9l2,14l52,8l130.5,-7.5625z" fillOpacity="null" strokeOpacity="null" strokeWidth="1.5" stroke="#ffffff" fill="none"/>
                            <path stroke="#000" id="svg_51" d="m239.5,453.16992l-15.43003,-19.01367l-123.44022,-5.15625l-43.62974,1.03125l2.12828,30.9375l42.5656,1.03125l137.80611,-8.83008z" fillOpacity="null" strokeOpacity="null" strokeWidth="1.5" fill="#fff"/>
                            <path stroke="#000" id="svg_52" d="m283.49999,418.16992l-19.31924,-19.01367l-154.55392,-5.15625l-54.62682,1.03125l2.66471,30.9375l53.29447,1.03125l172.5408,-8.83008z" fillOpacity="null" strokeOpacity="null" strokeWidth="1.5" fill="#fff"/>
                            <path stroke="#000" id="svg_53" d="m263.49999,383.97265l-17.62828,-17.28515l-141.02621,-4.6875l-49.84548,0.9375l2.43148,28.125l48.62974,0.9375l157.43875,-8.02735z" fillOpacity="null" strokeOpacity="null" strokeWidth="1.5" fill="#fff"/>
                            <path stroke="#000" id="svg_54" d="m239.49999,353.97265l-15.76822,-17.28515l-126.14575,-4.6875l-44.586,0.9375l2.17492,28.125l43.49854,0.9375l140.82651,-8.02735z" fillOpacity="null" strokeOpacity="null" strokeWidth="1.5" fill="#fff"/>
                            <path stroke="#000" id="svg_55" d="m213.12824,322.97265l-13.65452,-17.28515l-109.23613,-4.6875l-38.60932,0.9375l1.88338,28.125l37.66763,0.9375l121.94896,-8.02735z" fillOpacity="null" strokeOpacity="null" strokeWidth="1.5" fill="#fff"/>
                            <path stroke="#000" id="svg_56" d="m198.49999,292.47395l-12.3863,-13.25195l-99.09036,-3.59375l-35.02331,0.71875l1.70845,21.5625l34.16909,0.71875l110.62243,-6.1543z" fillOpacity="null" strokeOpacity="null" strokeWidth="1.5" fill="#fff"/>
                            <path stroke="#000" id="svg_57" d="m179.49999,266.11328l-10.86443,-12.67578l-86.91544,-3.4375l-30.7201,0.6875l1.49854,20.625l29.97084,0.6875l97.03059,-5.88672z" fillOpacity="null" strokeOpacity="null" strokeWidth="1.5" fill="#fff"/>
                            <path stroke="#000" id="svg_58" d="m160.49999,241.11328l-9.25801,-12.67578l-74.06413,-3.4375l-26.17783,0.6875l1.27697,20.625l25.53935,0.6875l82.68365,-5.88672z" fillOpacity="null" strokeOpacity="null" strokeWidth="1.5" fill="#fff"/>
                            <path stroke="#000" id="svg_59" d="m144.49999,216.11328l-7.82069,-12.67578l-62.56559,-3.4375l-22.11369,0.6875l1.07872,20.625l21.57433,0.6875l69.84692,-5.88672z" fillOpacity="null" strokeOpacity="null" strokeWidth="1.5" fill="#fff"/>
                            <path stroke="#000" id="fps_hangarId0" d="m143.49999,167.11328l-7.82069,-12.67578l-62.56559,-3.4375l-22.11369,0.6875l1.07872,20.625l21.57433,0.6875l69.84692,-5.88672z" fillOpacity="null" strokeOpacity="null" strokeWidth="1.5" fill="#fff"/>
                            <path stroke="#000" id="fps_hangarId1" d="m151.49998,138.8457l-8.58162,-13.25195l-68.65305,-3.59375l-24.26529,0.71875l1.18368,21.5625l23.67345,0.71875l76.64283,-6.1543z" fillOpacity="null" strokeOpacity="null" strokeWidth="1.5" fill="#fff"/>
                            <path stroke="#000" id="fps_hangarId2" d="m165.49999,113.57812l-9.84984,-13.82812l-78.79883,-3.75l-27.8513,0.75l1.35861,22.5l27.17199,0.75l87.96937,-6.42188z" fillOpacity="null" strokeOpacity="null" strokeWidth="1.5" fill="#fff"/>
                            <path stroke="#000" id="fps_hangarId3" d="m174.49999,86.57812l-10.61077,-13.82812l-84.8863,-3.75l-30.0029,0.75l1.46355,22.5l29.27113,0.75l94.76529,-6.42188z" fillOpacity="null" strokeOpacity="null" strokeWidth="1.5" fill="#fff"/>
                            <path id="fps_hangarId4" d="m174.57919,61.31054l-10.70201,-14.40429l-85.61625,-3.90625l-30.26091,0.78125l1.47615,23.4375l29.52283,0.78125l95.58019,-6.68946z" fillOpacity="null" strokeOpacity="null" strokeWidth="1.5" fill="#fff" stroke="#000"/>
                            <path id="fps_hangarId5" d="m174.73757,37.38085l-10.79995,-12.0996l-86.39976,-3.28125l-30.53784,0.65625l1.48966,19.6875l29.79299,0.65625l96.4549,-5.61915z" fillOpacity="null" strokeOpacity="null" strokeWidth="1.5" fill="#fff" stroke="#000"/>
                            <path fill="#cccccc" strokeWidth="1.5" d="m185.86956,514.83092c0,0 3.10559,7.34299 2.9115,7.34299c0.19409,0 7.95807,0 7.95807,0c0,0 -2.71739,-8.26087 -2.91149,-8.26087c0.1941,0 -7.95808,0.91788 -7.95808,0.91788z" id="svg_4" stroke="#000000"/>
                            <path fill="#cccccc" strokeWidth="1.5" d="m185.86956,479.17875c0,0 3.10559,7.34299 2.9115,7.34299c0.19409,0 7.95807,0 7.95807,0c0,0 -2.71739,-8.26087 -2.91149,-8.26087c0.1941,0 -7.95808,0.91788 -7.95808,0.91788z" id="svg_10" stroke="#000000"/>
                            <path fill="#cccccc" strokeWidth="1.5" d="m185.86956,440.91788c0,0 3.10559,7.34299 2.9115,7.34299c0.19409,0 7.95807,0 7.95807,0c0,0 -2.71739,-8.26087 -2.91149,-8.26087c0.1941,0 -7.95808,0.91788 -7.95808,0.91788z" id="svg_11" stroke="#ffffff"/>
                            <path fill="#cccccc" strokeWidth="1.5" d="m185.86956,406.13527c0,0 3.10559,7.34299 2.9115,7.34299c0.19409,0 7.95807,0 7.95807,0c0,0 -2.71739,-8.26087 -2.91149,-8.26087c0.1941,0 -7.95808,0.91788 -7.95808,0.91788z" id="svg_12" stroke="#ffffff"/>
                            <path fill="#cccccc" strokeWidth="1.5" d="m185.86956,372.22223c0,0 3.10559,7.34299 2.9115,7.34299c0.19409,0 7.95807,0 7.95807,0c0,0 -2.71739,-8.26087 -2.91149,-8.26087c0.1941,0 -7.95808,0.91788 -7.95808,0.91788z" id="svg_13" stroke="#ffffff"/>
                            <path fill="#cccccc" strokeWidth="1.5" d="m185.86956,341.78745c0,0 3.10559,7.34299 2.9115,7.34299c0.19409,0 7.95807,0 7.95807,0c0,0 -2.71739,-8.26087 -2.91149,-8.26087c0.1941,0 -7.95808,0.91788 -7.95808,0.91788z" id="svg_14" stroke="#ffffff"/>
                            <path fill="#cccccc" strokeWidth="1.5" d="m185.86956,311.35267c0,0 3.10559,7.34299 2.9115,7.34299c0.19409,0 7.95807,0 7.95807,0c0,0 -2.71739,-8.26087 -2.91149,-8.26087c0.1941,0 -7.95808,0.91788 -7.95808,0.91788z" id="svg_15" stroke="#ffffff"/>
                            <line fill="none" stroke="#ffffff" strokeWidth="0.5" fillOpacity="null" x1="196.304348" y1="319.130435" x2="196.304348" y2="521.304348" id="svg_17" strokeLinejoin="null" strokeLinecap="null"/>
                            <line fill="none" stroke="#000" strokeWidth="0.5" strokeOpacity="null" fillOpacity="null" x1="336.73913" y1="493.478261" x2="338.478261" y2="494.782609" id="svg_19" strokeLinejoin="null" strokeLinecap="null"/>
                            <line fill="none" stroke="#ffffff" strokeWidth="0.5" fillOpacity="null" x1="196.304348" y1="319.130435" x2="196.304348" y2="521.304348" id="svg_20" strokeLinejoin="null" strokeLinecap="null"/>
                            <line fill="none" stroke="#ffffff" strokeWidth="0.5" fillOpacity="null" x1="196.304348" y1="319.130435" x2="196.304348" y2="521.304348" id="svg_21" strokeLinejoin="null" strokeLinecap="null"/>
                            <line fill="none" stroke="#ffffff" strokeWidth="0.5" fillOpacity="null" x1="189.782609" y1="319.130435" x2="189.782609" y2="521.304348" id="svg_22" strokeLinejoin="null" strokeLinecap="null"/>
                            <line fill="none" stroke="#ffffff" strokeWidth="0.5" fillOpacity="null" x1="187.173913" y1="313.478261" x2="187.173913" y2="515.652174" id="svg_23" strokeLinejoin="null" strokeLinecap="null"/>
                            <line fill="none" stroke="#ffffff" strokeWidth="0.5" fillOpacity="null" x1="187.608695" y1="313.043478" x2="187.608695" y2="515.217391" id="svg_24" strokeLinejoin="null" strokeLinecap="null"/>
                            <line fill="none" stroke="#ffffff" strokeWidth="0.5" fillOpacity="null" x1="192.391304" y1="312.173913" x2="192.391304" y2="514.347826" id="svg_25" strokeLinejoin="null" strokeLinecap="null"/>
                            <path fill="#cccccc" strokeWidth="1.5" fillOpacity="null" d="m58.17844,319.50243c0,0 2.5019,-0.17157 2.488,-0.17157c0.0139,0 0.31969,3.34571 0.30579,3.34571c0.0139,0 -2.7382,-0.04289 -2.7521,-0.04289c0.0139,0 -0.04169,-3.13125 -0.04169,-3.13125z" id="svg_31" stroke="#cccccc"/>
                            <path fill="#cccccc" strokeWidth="1.5" fillOpacity="null" d="m58.17843,310.95224c0,0 2.50191,-0.17157 2.48801,-0.17157c0.0139,0 0.31969,3.34571 0.30579,3.34571c0.0139,0 -2.7382,-0.04289 -2.7521,-0.04289c0.0139,0 -0.0417,-3.13125 -0.0417,-3.13125z" id="svg_35" stroke="#cccccc"/>
                            <path fill="#cccccc" strokeWidth="1.5" fillOpacity="null" d="m58.17843,319.50243c0,0 2.50191,-0.17157 2.48801,-0.17157c0.0139,0 0.31969,3.34571 0.30579,3.34571c0.0139,0 -2.7382,-0.04289 -2.7521,-0.04289c0.0139,0 -0.0417,-3.13125 -0.0417,-3.13125z" id="svg_36" stroke="#cccccc"/>
                            <path fill="#cccccc" strokeWidth="1.5" fillOpacity="null" d="m58.17843,310.95224c0,0 2.50191,-0.17157 2.48801,-0.17157c0.0139,0 0.31969,3.34571 0.30579,3.34571c0.0139,0 -2.7382,-0.04289 -2.7521,-0.04289c0.0139,0 -0.0417,-3.13125 -0.0417,-3.13125z" id="svg_37" stroke="#cccccc"/>
                            <path fill="#cccccc" strokeWidth="1.5" fillOpacity="null" d="m58.17843,290.1344c0,0 2.50191,-0.17157 2.48801,-0.17157c0.0139,0 0.31969,3.34571 0.30579,3.34571c0.0139,0 -2.7382,-0.04289 -2.7521,-0.04289c0.0139,0 -0.0417,-3.13125 -0.0417,-3.13125z" id="svg_38" stroke="#cccccc"/>
                            <path fill="#cccccc" strokeWidth="1.5" fillOpacity="null" d="m58.17843,281.58421c0,0 2.50191,-0.17157 2.48801,-0.17157c0.0139,0 0.31969,3.34571 0.30579,3.34571c0.0139,0 -2.7382,-0.04289 -2.7521,-0.04289c0.0139,0 -0.0417,-3.13125 -0.0417,-3.13125z" id="svg_39" stroke="#cccccc"/>
                            <path fill="#cccccc" strokeWidth="1.5" fillOpacity="null" d="m58.17843,263.74035c0,0 2.50191,-0.17157 2.48801,-0.17157c0.0139,0 0.31969,3.34571 0.30579,3.34571c0.0139,0 -2.7382,-0.04289 -2.7521,-0.04289c0.0139,0 -0.0417,-3.13125 -0.0417,-3.13125z" id="svg_40" stroke="#cccccc"/>
                            <path fill="#cccccc" strokeWidth="1.5" fillOpacity="null" d="m58.17843,255.19016c0,0 2.50191,-0.17157 2.48801,-0.17157c0.0139,0 0.31969,3.34571 0.30579,3.34571c0.0139,0 -2.7382,-0.04289 -2.7521,-0.04289c0.0139,0 -0.0417,-3.13125 -0.0417,-3.13125z" id="svg_41" stroke="#cccccc"/>
                            <path fill="#cccccc" strokeWidth="1.5" fillOpacity="null" d="m58.17843,238.83329c0,0 2.50191,-0.17157 2.48801,-0.17157c0.0139,0 0.31969,3.34571 0.30579,3.34571c0.0139,0 -2.7382,-0.04289 -2.7521,-0.04289c0.0139,0 -0.0417,-3.13125 -0.0417,-3.13125z" id="svg_42" stroke="#cccccc"/>
                            <path fill="#cccccc" strokeWidth="1.5" fillOpacity="null" d="m58.17843,230.2831c0,0 2.50191,-0.17157 2.48801,-0.17157c0.0139,0 0.31969,3.34571 0.30579,3.34571c0.0139,0 -2.7382,-0.04289 -2.7521,-0.04289c0.0139,0 -0.0417,-3.13125 -0.0417,-3.13125z" id="svg_43" stroke="#cccccc"/>
                            <path fill="#cccccc" strokeWidth="1.5" fillOpacity="null" d="m58.17843,213.55447c0,0 2.50191,-0.17157 2.48801,-0.17157c0.0139,0 0.31969,3.34571 0.30579,3.34571c0.0139,0 -2.7382,-0.04289 -2.7521,-0.04289c0.0139,0 -0.0417,-3.13125 -0.0417,-3.13125z" id="svg_44" stroke="#cccccc"/>
                            <path fill="#cccccc" strokeWidth="1.5" fillOpacity="null" d="m58.17843,205.00429c0,0 2.50191,-0.17157 2.48801,-0.17157c0.0139,0 0.31969,3.34571 0.30579,3.34571c0.0139,0 -2.7382,-0.04289 -2.7521,-0.04289c0.0139,0 -0.0417,-3.13125 -0.0417,-3.13125z" id="svg_45" stroke="#cccccc"/>
                            <path fill="#cccccc" strokeWidth="1.5" fillOpacity="null" d="m58.17843,164.48384c0,0 2.50191,-0.17157 2.48801,-0.17157c0.0139,0 0.31969,3.34571 0.30579,3.34571c0.0139,0 -2.7382,-0.04289 -2.7521,-0.04289c0.0139,0 -0.0417,-3.13125 -0.0417,-3.13125z" id="svg_48" stroke="#cccccc"/>
                            <path fill="#cccccc" strokeWidth="1.5" fillOpacity="null" d="m58.17843,155.93365c0,0 2.50191,-0.17157 2.48801,-0.17157c0.0139,0 0.31969,3.34571 0.30579,3.34571c0.0139,0 -2.7382,-0.04289 -2.7521,-0.04289c0.0139,0 -0.0417,-3.13125 -0.0417,-3.13125z" id="svg_49" stroke="#cccccc"/>
                            <path fill="#cccccc" strokeWidth="1.5" fillOpacity="null" d="m58.17843,136.60279c0,0 2.50191,-0.17157 2.48801,-0.17157c0.0139,0 0.31969,3.34571 0.30579,3.34571c0.0139,0 -2.7382,-0.04289 -2.7521,-0.04289c0.0139,0 -0.0417,-3.13125 -0.0417,-3.13125z" id="svg_50" stroke="#cccccc"/>
                            <path fill="#cccccc" strokeWidth="1.5" fillOpacity="null" d="m58.17843,128.0526c0,0 2.50191,-0.17157 2.48801,-0.17157c0.0139,0 0.31969,3.34571 0.30579,3.34571c0.0139,0 -2.7382,-0.04289 -2.7521,-0.04289c0.0139,0 -0.0417,-3.13125 -0.0417,-3.13125z" id="fps_hangarId6" stroke="#cccccc"/>
                            <path fill="#cccccc" strokeWidth="1.5" fillOpacity="null" d="m58.17843,110.58048c0,0 2.50191,-0.17157 2.48801,-0.17157c0.0139,0 0.31969,3.34571 0.30579,3.34571c0.0139,0 -2.7382,-0.04289 -2.7521,-0.04289c0.0139,0 -0.0417,-3.13125 -0.0417,-3.13125z" id="fps_hangarId7" stroke="#cccccc"/>
                            <path fill="#cccccc" strokeWidth="1.5" fillOpacity="null" d="m58.17843,102.03029c0,0 2.50191,-0.17157 2.48801,-0.17157c0.0139,0 0.31969,3.34571 0.30579,3.34571c0.0139,0 -2.7382,-0.04289 -2.7521,-0.04289c0.0139,0 -0.0417,-3.13125 -0.0417,-3.13125z" id="fps_hangarId8" stroke="#cccccc"/>
                            <path fill="#cccccc" strokeWidth="1.5" fillOpacity="null" d="m58.17843,83.81468c0,0 2.50191,-0.17157 2.48801,-0.17157c0.0139,0 0.31969,3.34571 0.30579,3.34571c0.0139,0 -2.7382,-0.04289 -2.7521,-0.04289c0.0139,0 -0.0417,-3.13125 -0.0417,-3.13125z" id="fps_hangarId9" stroke="#cccccc"/>
                            <path fill="#cccccc" strokeWidth="1.5" fillOpacity="null" d="m58.17843,75.26449c0,0 2.50191,-0.17157 2.48801,-0.17157c0.0139,0 0.31969,3.34571 0.30579,3.34571c0.0139,0 -2.7382,-0.04289 -2.7521,-0.04289c0.0139,0 -0.0417,-3.13125 -0.0417,-3.13125z" id="svg_70" stroke="#cccccc"/>
                            <path fill="#cccccc" strokeWidth="1.5" fillOpacity="null" d="m58.17843,58.16412c0,0 2.50191,-0.17157 2.48801,-0.17157c0.0139,0 0.31969,3.34571 0.30579,3.34571c0.0139,0 -2.7382,-0.04289 -2.7521,-0.04289c0.0139,0 -0.0417,-3.13125 -0.0417,-3.13125z" id="svg_71" stroke="#cccccc"/>
                            <path fill="#cccccc" strokeWidth="1.5" fillOpacity="null" d="m58.17843,49.61393c0,0 2.50191,-0.17157 2.48801,-0.17157c0.0139,0 0.31969,3.34571 0.30579,3.34571c0.0139,0 -2.7382,-0.04289 -2.7521,-0.04289c0.0139,0 -0.0417,-3.13125 -0.0417,-3.13125z" id="svg_72" stroke="#cccccc"/>
                            <path fill="#cccccc" strokeWidth="1.5" fillOpacity="null" d="m58.17843,35.1158c0,0 2.50191,-0.17157 2.48801,-0.17157c0.0139,0 0.31969,3.34571 0.30579,3.34571c0.0139,0 -2.7382,-0.04289 -2.7521,-0.04289c0.0139,0 -0.0417,-3.13125 -0.0417,-3.13125z" id="svg_73" stroke="#cccccc"/>
                            <path fill="#cccccc" strokeWidth="1.5" fillOpacity="null" d="m58.17843,26.56561c0,0 2.50191,-0.17157 2.48801,-0.17157c0.0139,0 0.31969,3.34571 0.30579,3.34571c0.0139,0 -2.7382,-0.04289 -2.7521,-0.04289c0.0139,0 -0.0417,-3.13125 -0.0417,-3.13125z" id="svg_74" stroke="#cccccc"/>
                            <line fill="none" strokeWidth="0.25" fillOpacity="null" x1="61.152419" y1="323.048323" x2="61.524161" y2="29.739794" id="svg_75" strokeLinejoin="null" strokeLinecap="null" stroke="#ffffff"/>
                            <line fill="none" strokeWidth="0.25" fillOpacity="null" x1="58.550189" y1="323.048323" x2="58.92193" y2="29.739794" id="svg_76" strokeLinejoin="null" strokeLinecap="null" stroke="#ffffff"/>
                            <line fill="none" strokeWidth="0.25" fillOpacity="null" x1="58.550189" y1="319.330851" x2="58.92193" y2="26.022322" id="svg_77" strokeLinejoin="null" strokeLinecap="null" stroke="#ffffff"/>
                            <line fill="none" strokeWidth="0.25" fillOpacity="null" x1="60.408925" y1="319.330851" x2="60.780667" y2="26.022322" id="svg_78" strokeLinejoin="null" strokeLinecap="null" stroke="#ffffff"/>
                            <path fill="#7f3f00" stroke="#ffffff" strokeWidth="0.25" d="m184.30079,455.93667c0,0 -8.17942,-22.69129 -8.17942,-22.69129c0,0 -76.2533,-3.95778 -76.38522,-4.22163c0.13192,0.26385 -41.82058,1.31926 -41.82058,1.31926c0,0 2.11082,30.07915 1.97889,30.07915c0.13193,0 41.82058,1.58312 42.08443,1.58312c0.26386,0 82.3219,-6.06861 82.3219,-6.06861z" onClick={(e) => this.handleDeckClick('generalStore', e)} name="generalStore" id="fps_generalStoreId"/>
                            <path fill="#00ffff" stroke="#ffffff" strokeWidth="0.25" d="m237.93075,452.37017c0,0 -14.43963,-17.67239 -14.43963,-17.67239c0,0 -24.78446,-0.86207 -24.78446,-0.86207c0,0 6.24999,21.12067 6.24999,20.90515c0,-0.21552 32.9741,-2.37069 32.9741,-2.37069z" onClick={(e) => this.handleDeckClick('salon', e)} name="salon" id="fps_salonId"/>
                            <path fill="#00bf5f" stroke="#ffffff" strokeWidth="0.25" strokeOpacity="null" fillOpacity="null" opacity="0.5" d="m110.12918,426.29262c0,0 -51.72408,-1.2931 -51.72408,-1.2931" id="svg_82"/>
                            <path fill="#007f3f" stroke="#ffffff" strokeWidth="0.25" strokeOpacity="null" d="m110.34469,426.0771c0,0 -51.72408,-0.86207 -51.72408,-0.86207c0,0 -2.5862,-29.52583 -2.5862,-29.52583c0,0 52.80166,-1.07758 52.80166,-1.07758c0,0 68.31889,2.37069 68.31889,2.37069c0,0 7.97413,25.21548 7.75862,25.21547c0.21551,0.00001 -74.56889,3.87932 -74.56889,3.87932z" onClick={(e) => this.handleDeckClick('greenhouse', e)} name="greenhouse" id="fps_greenhouseId"/>
                            <path fill="#007f3f" stroke="#ffffff" strokeWidth="0.25" strokeOpacity="null" opacity="0.5" d="m179.52565,405.38747" id="svg_84"/>
                            <path fill="#007f3f" stroke="#ffffff" strokeWidth="0.25" strokeOpacity="null" d="m178.66358,402.15471c0,0 25.86204,0.64656 25.86204,0.64656c0,0 -3.01724,-5.17241 -3.23275,-5.17242c0.21551,0.00001 -24.35343,-0.86206 -24.35343,-0.86206c0,0 1.72414,5.38792 1.72414,5.38792z" id="svg_85"/>
                            <path fill="#007f3f" stroke="#ffffff" strokeWidth="0.25" strokeOpacity="null" d="m204.3101,402.80127c0,0 -3.23275,-5.38793 -3.44827,-5.38794c0.21552,0.00001 63.14648,2.58621 62.93097,2.5862c0.21551,0.00001 4.31033,4.31035 4.09482,4.31034c0.21551,0.00002 -63.57752,-1.5086 -63.57752,-1.5086z" id="svg_86"/>
                            <path fill="#007f3f" stroke="#ffffff" strokeWidth="0.25" strokeOpacity="null" d="m204.52562,402.80127c0,0 10.1293,18.10342 10.1293,18.10342c0,0 66.81027,-3.23275 66.59475,-3.23276c0.21552,0.00001 -13.36205,-13.57756 -13.57757,-13.57757c0.21552,0.00001 -63.14648,-1.29309 -63.14648,-1.29309z" id="svg_87"/>
                            <path fill="#ff7f00" stroke="#ffffff" strokeWidth="0.25" strokeOpacity="null" d="m183.18944,387.28404c0,0 -75.86198,3.87931 -76.0775,3.8793c0.21552,0.00001 -48.92236,-0.86206 -48.92236,-0.86206c0,0 -2.15517,-26.50859 -2.15517,-26.50859c0,0 47.41374,-1.2931 47.41374,-1.2931c0,0 68.5344,2.5862 68.31889,2.58619c0.21551,0.00001 11.4224,22.19826 11.4224,22.19826z" onClick={(e) => this.handleDeckClick('labMaterials', e)} name="labsMaterials" id="fps_labsMaterialsId"/>
                            <path fill="#bfbf00" stroke="#ffffff" strokeWidth="0.25" strokeOpacity="null" d="m182.54289,356.46511c0,0 -84.05163,4.74137 -84.26714,4.74136c0.21551,0.00001 -42.45686,-1.07757 -42.67237,-1.07758c0.21551,0.00001 -1.93966,-26.50858 -2.15517,-26.50859c0.21551,0.00001 43.74995,-1.29309 43.74995,-1.29309c0,0 77.3706,3.01723 77.3706,3.01723c0,0 7.97413,21.12067 7.97413,21.12067z" onClick={(e) => this.handleDeckClick('labPortalConstruction', e)} name="labPortal" id="fps_labsPortalId"/>
                            <path fill="#aa56ff" stroke="#ffffff" strokeWidth="0.25" strokeOpacity="null" fillOpacity="null" d="m182.7584,324.13756c0,0 -7.54309,-18.53446 -7.7586,-18.53447c0.21551,0.00001 -84.69819,-4.09481 -84.69819,-4.09481c0,0 -26.07755,0.64655 -26.29306,0.64654c0.21551,0.00001 2.15516,27.58618 2.15516,27.58618c0,0 27.80169,1.29311 27.80169,1.29311c0,0 88.793,-6.89655 88.793,-6.89655z" onClick={(e) => this.handleDeckClick('cafeteria', e)} name="cafeteria" id="fps_cafeteriaId"/>
                            <path fill="#7f0000" stroke="#ffffff" strokeWidth="0.25" strokeOpacity="null" d="m66.37923,297.19794c0,0 -3.01724,-20.2586 -3.01724,-20.2586c0,0 23.49135,-0.86207 23.49135,-0.86207c0,0 98.9223,3.87931 98.9223,3.87931c0,0 10.77585,11.63791 10.77585,11.63791c0,0 -107.32746,7.11207 -107.54297,7.11206c0.21551,0.00001 -22.62929,-1.50861 -22.62929,-1.50861z" onClick={(e) => this.handleDeckClick('labEnvironmentSimulation', e)} name="labEnvSimulation" id="fps_labEnvSimulationId"/>
                            <path fill="#0000ff" stroke="#ffffff" strokeWidth="0.25" strokeOpacity="null" d="m65.73268,270.68935c0,-0.21552 -3.01724,-19.61205 -3.01724,-19.61205c0,0 19.39653,-0.43103 19.39653,-0.43103c0,0 85.99128,3.23275 85.99128,3.23275c0,0 10.1293,11.4224 10.1293,11.4224c0,0 -94.61196,6.68103 -94.82747,6.68102c0.21551,0.00001 -17.6724,-1.07758 -17.6724,-1.29309z" onClick={(e) => this.handleDeckClick('library', e)} name="library" id="fps_libraryId"/>
                            <path fill="#007f7f" stroke="#ffffff" strokeWidth="0.25" strokeOpacity="null" d="m66.59474,245.68938c0,0 -1.72413,-19.61205 -1.93964,-19.61206c0.21551,0.00001 11.42239,-0.64654 11.42239,-0.64654c0,0 74.7844,3.23275 74.7844,3.44827c0,0.21552 8.62068,11.4224 8.40517,11.42239c0.21551,0.00001 -80.38785,6.46552 -80.60336,6.46551c0.21551,0.00001 -12.06896,-1.07757 -12.06896,-1.07757z" onClick={(e) => this.handleDeckClick('security', e)} name="security" id="fps_securityId"/>
                            <path fill="#ffff00" stroke="#ffffff" strokeWidth="0.25" strokeOpacity="null" fillOpacity="null" d="m143.04197,215.37201c0,0 -6.95792,-11.48867 -6.95792,-11.48867c0,0 -61.65044,-3.55986 -61.65044,-3.55986c0,0 -10.5178,0.80906 -10.5178,0.80906c0,0 0.48544,19.57927 0.48544,19.41746c0,0.16181 11.65048,0.97087 11.65047,0.80906c0.00001,0.16181 66.99025,-5.98705 66.99025,-5.98705z" onClick={(e) => this.handleDeckClick('smallBank', e)} name="smallBank" id="fps_smallBankId"/>
                            <path fill="#ff007f" strokeWidth="0.25" strokeOpacity="null" d="m72.00642,151.77982c0,0 62.78313,3.13743 62.78313,3.13743c0,0 7.11974,11.29473 7.11973,11.13787c0.00001,0.15686 -68.28474,6.27485 -68.28474,6.27485c0,0 -1.61812,-20.55015 -1.61812,-20.55015z" onClick={(e) => this.handleDeckClick('labSignalAnalysis', e)} name="labSignals" id="fps_labsSignalsId" stroke="#ffffff"/>
                            <path fill="#00007f" stroke="#ffffff" strokeWidth="0.25" strokeOpacity="null" d="m73.78636,122.65363c0,0 68.93199,3.55986 68.93199,3.55986c0,0 7.60517,11.81229 7.60516,11.65049c0.00001,0.16181 -74.91903,6.63429 -74.91903,6.63429c0,0 -1.61812,-21.84464 -1.61812,-21.84464z" onClick={(e) => this.handleDeckClick('crewQuarters', e)} name="crewQuarters" id="fps_crewQuarters1Id" transform="rotate(0.09373591095209122 112.0549316406013,133.5759582519542) "/>
                            <path fill="#cccccc" stroke="#ffffff" strokeWidth="0.25" strokeOpacity="null" fillOpacity="null" opacity="0.5" d="m77.66985,119.41738c0,0 -1.61812,-23.13914 -1.61812,-23.13914" id="svg_100"/>
                            <path fill="#00007f" stroke="#ffffff" strokeWidth="0.25" d="m77.83166,119.09376c0,0 -1.61812,-22.49189 -1.61812,-22.6537c0,0.16181 78.80253,3.88349 78.80253,3.72168c0,0.16181 8.73786,12.62134 8.73786,12.62134c0,0 -85.92227,6.31068 -85.92227,6.31068z" onClick={(e) => this.handleDeckClick('crewQuarters', e)} name="crewQuarters" id="fps_crewQuarters2Id"/>
                            <path fill="#00007f" stroke="#ffffff" strokeWidth="0.25" strokeOpacity="null" d="m79.77341,92.07112c0,0 -0.80906,-22.6537 -0.80906,-22.6537c0,0 84.78958,3.88349 84.78958,3.88349c0,0 9.06148,12.45954 9.06148,12.45954c0,0 -93.042,6.31067 -93.042,6.31067z" onClick={(e) => this.handleDeckClick('crewQuarters', e)} name="crewQuarters" id="fps_crewQuarters3Id"/>
                            <path fill="#ff00ff" strokeWidth="0.25" strokeOpacity="null" d="m81.23994,67.31386c0,0 -1.79016,-23.94821 -1.79016,-23.94821c0,0 83.98796,4.20712 83.98796,4.20712c0,0 9.54747,12.94498 9.54747,12.94498c0,0 -91.74528,6.79611 -91.74528,6.79611l0.00001,0z" onClick={(e) => this.handleDeckClick('gym', e)} name="gym" id="fps_gymId" stroke="#ffffff"/>
                            <path fill="#3f007f" strokeWidth="0.25" strokeOpacity="null" d="m66.17883,42.23297c0,0 -1.61577,-19.09384 -1.61577,-19.25564c0,0.1618 13.73402,-0.64726 13.73402,-0.80906c0,0.1618 86.71284,3.72167 86.71284,3.72167c0,0 9.15601,10.6796 9.15601,10.6796c0,0 -94.52238,6.31068 -94.52238,6.31068" onClick={(e) => this.handleDeckClick('observationDeck', e)} name="observatory" id="fps_observatoryId" stroke="#ffffff"/>
                            <text stroke="#3f007f"  textAnchor="start" fontFamily="Helvetica, Arial, sans-serif" fontSize="14" id="svg_3" y="34.0375" x="188.5" fill="#000000">Observatory</text>
                            <text stroke="#ff00ff"  textAnchor="start" fontFamily="Helvetica, Arial, sans-serif" fontSize="14" id="svg_5" y="56.0375" x="190.5" strokeWidth="0.25" fill="#000000">Gym</text>
                            <text  textAnchor="start" fontFamily="Helvetica, Arial, sans-serif" fontSize="14" id="svg_7" y="109.0375" x="192.5" stroke="#00007f" fill="#000000">Crew</text>
                            <text  textAnchor="start" fontFamily="Helvetica, Arial, sans-serif" fontSize="14" id="svg_8" y="166.0375" x="192.5" stroke="#ff007f" fill="#000000">Lab - Signal Analysis</text>
                            <text  textAnchor="start" fontFamily="Helvetica, Arial, sans-serif" fontSize="14" id="svg_9" y="212.0375" x="192.5" strokeWidth="null" stroke="#ffff00" fill="#000000">Small Bank</text>
                            <text  textAnchor="start" fontFamily="Helvetica, Arial, sans-serif" fontSize="14" id="svg_16" y="237.0375" x="191.5" strokeWidth="0.5" stroke="#007f7f" fill="#000000">Security</text>
                            <text  textAnchor="start" fontFamily="Helvetica, Arial, sans-serif" fontSize="14" id="svg_26" y="263.0375" x="192.5" strokeWidth="0.25" stroke="#0000ff" fill="#000000">Library</text>
                            <text  textAnchor="start" fontFamily="Helvetica, Arial, sans-serif" fontSize="14" id="svg_27" y="289.0375" x="213.5" strokeWidth="0.25" stroke="#7f0000" fill="#7f0000">Lab  - Env Simulation</text>
                            <text  textAnchor="start" fontFamily="Helvetica, Arial, sans-serif" fontSize="14" id="svg_29" y="317.0375" x="238.5" strokeWidth="0.5" stroke="#aa56ff" fill="#000000">Cafeteria</text>
                            <text  textAnchor="start" fontFamily="Helvetica, Arial, sans-serif" fontSize="14" id="svg_30" y="348.0375" x="256.5" strokeWidth="0.5" stroke="#bfbf00" fill="#000000">Lab - Portal Constr.</text>
                            <text  textAnchor="start" fontFamily="Helvetica, Arial, sans-serif" fontSize="14" id="svg_32" y="379.0375" x="277.5" strokeWidth="0.5" stroke="#ff7f00" fill="#000000">Lab - Materials</text>
                            <text  textAnchor="start" fontFamily="Helvetica, Arial, sans-serif" fontSize="14" id="svg_34" y="411.0375" x="296.5" fillOpacity="null" strokeWidth="0.5" stroke="#007f3f" fill="#000000">Greenhouse</text>
                            <text transform="rotate(90 367.24218750000006,485.2421875) " stroke="#004089"  textAnchor="start" fontFamily="Helvetica, Arial, sans-serif" fontSize="14" id="svg_81" y="490.0375" x="325.5" fillOpacity="null" strokeWidth="0.5" fill="#000000">Docking Ring</text>
                            <text transform="rotate(90 300.92968750000006,483.2421875) "  textAnchor="start" fontFamily="Helvetica, Arial, sans-serif" fontSize="14" id="svg_90" y="488.0375" x="277.5" fillOpacity="null" strokeWidth="0.5" stroke="#acd4ff" fill="#000000">Hangar</text>
                            <text  textAnchor="start" fontFamily="Helvetica, Arial, sans-serif" fontSize="14" id="svg_95" y="557.0375" x="78.5" fillOpacity="null" strokeWidth="0.5" stroke="#ffffff" fill="#000000">Engineering &amp; Cargo</text>
                            </g>
                        </svg>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default FarPortalStation;