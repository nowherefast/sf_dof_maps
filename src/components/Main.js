import React from 'react';

/** CSS */
import '../css/Main.css'


/** REACT Routing DOM */
import { BrowserRouter, Route} from 'react-router-dom'
import { LinkContainer } from 'react-router-bootstrap';

/** REACT Bootstrap Stuff*/
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Form from  'react-bootstrap/Form';
import ListGroup from 'react-bootstrap/ListGroup';
import InputGroup from 'react-bootstrap/InputGroup';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Image from 'react-bootstrap/Image';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import ProgressBar from 'react-bootstrap/ProgressBar';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';

/** Maps */
import FarPortalStation from './FarPortalStation.js';

class Main extends React.Component {
    constructor(props){
       super(props);
       this.state = {
       };
    }
    render() {
        return (
  
            <Container fluid={true}>
                <BrowserRouter>
                <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                    <Navbar.Brand href="/">Dawn of Flames</Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="mr-auto">
                        <NavDropdown title="Maps" id="collasible-nav-dropdown">
                            <LinkContainer to="/far_portal_station">
                                <NavDropdown.Item href="#">Far Portal Station</NavDropdown.Item>
                            </LinkContainer>
                        </NavDropdown>
                        </Nav>
                        <Nav>
                        <Nav.Link href="#deets">Something Will Go Here Eventually...</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
                <Route exact path = "/far_portal_station" component = {FarPortalStation} />
                </BrowserRouter>
                


            </Container>
        );
    }
}

export default Main;